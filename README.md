# covid-19-fr-socio-demographics

This repository includes all the codes that were used to produced the data shared at:

https://www.kaggle.com/fplandes/covid19-granular-demographics-and-times-series/

#### Credits

Project initiated by **François Landes** and **Victor Alfonso**.

## Structure of folders

- `data/` contains the data, as uploaded to the kaggle page.
- `preprocessing_static_data/`  contains:
    - raw data as downloaded from institutional sources: one source per folder, each folder containing
        - the raw data: the unzipped files needed (when it's zipped),
        - a small text file pointing to the original source, ending in `-sources.txt`.
    - in `PREPROCESSING/` , the .ipynb used to pre-process these raw data into compatible files, and the .ipynb to merge them all into a single file.
    - a `temporary_files/` folder used to separate individual pre-processings from the global merge.
    - in `POSTPROCESSING/` , a .ipynb may be used to read the final static data in `data/departments_static_data.csv` and produced a normalized data set, `departments_static_data_divBySubPop.csv`, which transforms absolute numbers in rates (giving percentages of population that has this or that feature instead of giving the total number of people with this or that feature). This post-processing is also performed in the *starter kernel* in Kaggle.
- `updating_timeseries_data/` contains the codes to download, pre-process, split/merge and write to files all the time-series kind of data. These, by definition, are updated regularly, and **one may need to use this code to get the most up-to-date data sets**.

## Content

It's **best to go to kaggle to take a look at the data**.

However, we mention here that our format is (in all files):
- Each row corresponds to a department.
- The "code" column corresponds to the department code (mostly numbers except for Corsica which is '2A' and '2B').
- The other columns are 'features' (in terms of ML language).
    - For the time series, these are dates, and the nature of the data is in the filename
    - For the static data, each column name describes the content of the column

## Comments

If you have ideas for additional data sources, and in particular if you want to share with us your code for pre-processing that data into 'our' format, please contact us, we'll be happy to add your contributions (along with proper credit, of course). If you want to contribute more, we are also open to adding you as developper in this repo.
