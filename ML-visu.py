#!/usr/bin/env python
# coding: utf-8

from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt # plotting
import numpy as np # linear algebra
import os # accessing directory structure
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)

import sklearn.neural_network
import sklearn.decomposition
import sklearn.multioutput
import sklearn.ensemble

import statsmodels.api

# import module_population_normalization

plt.ion()

# ndates_output = 14 #days
ndates_output = 2 #weeks
n_components = 9 ## PCA

pca_stdy = True
pca_stdy = False

print("inputType: combined, static-only, time-series-only")
# inputType="static-only"
inputType="time-series-only"
inputType="combined"

# modelType='arima' # not working yet
modelType='meta-mlp' # worse than mlp  (independent output channels)
modelType='meta-rfr' # worse (independent output channels)
modelType='svr'  # quite bad
modelType='rfr'  # ok-ish
modelType='mlp'

ndepartments = 100
nvalues = 24

things_useful_for_going_back_to_raw_numbers = []

path_to_inputs = '../kaggle/input/covid19-granular-demographics-and-times-series/'
input_filename1="departments_static_data_divBySubPop.csv"
input_filename2="time_series_covid_incidence_divBySubPop.csv"
input_filenameNORM="population_used_for_normalization.csv"


## Predictions : very simple models (not taking temporal aspect into account)
## consider  also sklearn.model_selection.TimeSeriesSplit(n_splits=5, max_train_size=None)

########################################################################################################################

def def_pop_cols(dfpop):
    ## get all columns which relate to population
    pop_cols = []
    for col in dfpop.columns[2:]:
        if col[:3] == "Pop":
            pop_cols.append(col)
    return pop_cols

def get_sex_age(col):
    ## reads the column names and extract the tags are integers
    if "sex=H" in col:
        sex=1
    elif "sex=F" in col:
        sex=2
    elif "sex=all" in col:
        sex=0
    else:
        print("weird: ", col)
        sex=0

    if "age" in col:
        if "agemin" in col:
            agemin = int(col.split("agemin=")[1].split("_")[0])
            if "agemax" in col:
                agemax = int(col.split("agemax=")[1].split("_")[0])
            else:
                print("VERY VERY weird !", col)
        elif "age=all" in col:
            agemin=0
            agemax=150
        else:
            print("weird: ", col)
            agemin=0
            agemax=150
    else:
        print("a little but not really weird: ", col)
        agemin=0
        agemax=150
    return sex, agemin, agemax

def get_sub_pop_corresponding_to_col(col, pop_cols, dfpop):
    ## returns the corresponding sub-population (correct denominator) of any column.
    ## If no exact match is found, returns the global population
    sex, agemin, agemax = get_sex_age(col)
    token = False
    for pop_col in pop_cols:
        Psex, Pagemin, Pagemax = get_sex_age(pop_col)
        if Psex == sex :
            if Pagemin == agemin:
                if Pagemax == agemax:
                    pop = dfpop[["code", pop_col]] ## we use the appropriate sub-pop as denominator
                    token = True
                    break

    if token == True:
        pass
    else:
        ## no match: we divide by the total pop of the dept (all sex, all age)
        print("no match: ", col)
        pop = dfpop[["code", "Pop_sex=all_age=all_Population"]]
    return pop

def normalize(dfIN, dfpop):
    pop_cols = def_pop_cols(dfpop)
    df3 = dfIN.copy() # pd.DataFrame()
    for col in df3.columns[2:]:
        ## Nbre columns are divided by the correct sub-pop.
        if col[:4] == "Nbre" :
            pop = get_sub_pop_corresponding_to_col(col, pop_cols, dfpop)
            for date in df3.date.unique():
                df3.loc[(df3.date==date), col] = (df3.loc[df3.date==date, col]).values / pop.iloc[:,1].values
            df3 = df3.rename(columns={col: "Rate"+col[4:]})
    return df3

def invert_norm(Rate_cols, dfpop):
    pop_cols = def_pop_cols(dfpop)
    pops =[]
    for col in Rate_cols:
        if col[:4] == "Rate" :
            pop = get_sub_pop_corresponding_to_col(col, pop_cols, dfpop)
            pops.append(pop.sort_values(by='code').iloc[:,1].values)
        #     for date in df3.date.unique():
        #         df3.loc[(df3.date==date), col] = (df3.loc[df3.date==date, col]).values / pop.iloc[:,1].values
        # df3 = df3.rename(columns={col: "Rate"+col[4:]})
    return np.array(pops).transpose()

########################################################################################################################

####################
### Loading data ###

## static data
df1 = pd.read_csv(path_to_inputs+input_filename1, delimiter=',')
df1 = df1.sort_values(by=['code'])
codes_static_file   =  np.array(df1.iloc[:,0])
regions_static_file =  np.array(df1.iloc[:,1])

## dynamic data (to be predicted)
df2 = pd.read_csv(path_to_inputs+input_filename2)
codes_dynamic_file = np.array(df2.iloc[::nvalues,0])
dynamic_file_column_names = np.array(df2.iloc[:nvalues,1])

dates_dynamics_file = df2.columns[2:]

## checking that tables are matched ##
assert(np.array_equal(codes_dynamic_file, codes_static_file)) # departmental codes
assert(codes_dynamic_file.shape[0]==ndepartments)
assert(dynamic_file_column_names.shape[0]==nvalues)

## normalization factors (population)
df5 = pd.read_csv(path_to_inputs+input_filenameNORM)
things_useful_for_going_back_to_raw_numbers.append(df5)
# pops = module_population_normalization.invert_norm(dynamic_file_column_names, df5)
pops = invert_norm(dynamic_file_column_names, df5)

######################################################
### Pre-processing 1: Dealing with MISSING VALUES  ###

# We could replace NaNs with the mean value of their column
df1 = df1.fillna(df1.mean())

df2 = df2.fillna(0)



######################################################################
### Pre-processing 2: export to numpy / reshaping the dynamic data ###
# Preparing DataFrame.s for export to numpy
# Numpy does not like string-index.
# But if we combine data sets using numpy, we have to be very careful that the index of departments (i.e. examples) do match.

## df1 : static
Xs_unscaled = np.array(df1.iloc[:,2:])

## df2 : dynamic (time series)
# (it is originally in a multi-index Pandas DataFrame)
raw_y_unshaped = np.array(df2.iloc[:,2:])
assert( raw_y_unshaped.shape[0]//ndepartments == nvalues)
raw_y_unshaped = raw_y_unshaped.reshape( (ndepartments, nvalues, raw_y_unshaped.shape[1]))
y_reshaped = []
for n in range(ndepartments) :
    y_reshaped.append(raw_y_unshaped[n].transpose().copy())
y_reshaped = np.array(y_reshaped)
del raw_y_unshaped



###################################
### standardizing (static only) ###

## standardize static data: as usual
mean_Xs = Xs_unscaled.mean(axis=0)
std_Xs  = Xs_unscaled.std (axis=0)
Xs_scaled = (Xs_unscaled-mean_Xs)/std_Xs

things_useful_for_going_back_to_raw_numbers.append(mean_Xs)
things_useful_for_going_back_to_raw_numbers.append(std_Xs )


##############################
### 7-days rolling average ###
## this will reduce noise, and kill the 7-days periodicity
nweeks = dates_dynamics_file.size//7
leftover_days = dates_dynamics_file.size%7
y_oncePerWeek = np.zeros( (ndepartments, nweeks, nvalues) )
y_oncePerWeek[:,0,:] = np.mean(y_reshaped[:,0:leftover_days+7,:], axis=1) ## the first (oldest) average is made on more days
# print(0, leftover_days+7)
for week in range(1,nweeks):
    y_oncePerWeek[:,week,:] = np.mean(y_reshaped[:,leftover_days+week*7:leftover_days+(week+1)*7,:], axis=1)
    # print(leftover_days+week*7, leftover_days+(week+1)*7)
del y_reshaped


##############################
### Pre-processing 4: PCA  ###
### or "how correlated are our entries ?"
# Let's do a PCA on the static (socio-demographics) indicators

def quick_look_at_pca(X, n_components):
    pca = sklearn.decomposition.PCA(n_components=n_components)
    pca.fit(X)
    Xp = pca.transform(X)
    plt.plot(pca.explained_variance_ratio_[:10], label="explained_variance_ratio")
    plt.legend()
    plt.xlabel("n_components (PCA)")
    plt.ylim([0,1])
    Xrecov = pca.inverse_transform(Xp)
    print("reconstruciton (Mean Absolute) Error: ", abs(X-Xrecov).mean())
    print("Xp.shape, pca.noise_variance_", Xp.shape, pca.noise_variance_)
    return Xp

def reconstruction_errors(X, ncomp_range):
    recos = []
    for n_components in ncomp_range:
        pca = sklearn.decomposition.PCA(n_components=n_components)
        pca.fit(X)
        Xp = pca.transform(X)
        Xrecov = pca.inverse_transform(Xp)
        reconstruction_MAE = abs(X-Xrecov).mean()
        recos.append( (n_components, reconstruction_MAE,pca.noise_variance_) )
    return np.array(recos)

if pca_stdy == True :
    n_components = None
    plt.figure(1)
    quick_look_at_pca(Xs_scaled, n_components)
    plt.show()

    plt.figure(2)
    ncomp_range = range(1, 20, 1)
    recos = reconstruction_errors(Xs_scaled, ncomp_range)
    plt.plot(recos[:,0],recos[:,1], marker='o', label="reconstruction error (MAE)")
    plt.plot(recos[:,0],recos[:,2], marker='o', label="noise variance")
    plt.xlabel("n_components (PCA)")
    plt.legend()
    plt.show()
    #### Static data correlations: Conclusion ####
    # The (static) inputs appear to be quite correlated (between them), but not so much




################################################
### Pre-processing 5: standardization (+PCA) ###

# We should keep track of those variables, to be able to re-scale our predictions back into their original form.

pca = sklearn.decomposition.PCA(n_components=n_components)
Xpca = pca.fit_transform(Xs_scaled)
things_useful_for_going_back_to_raw_numbers.append(pca)
del Xs_scaled

## y_oncePerWeek : dynamic data (all dates)
Xd  = y_oncePerWeek[:, :-ndates_output ].copy() ## dynamic data used as features
yd  = y_oncePerWeek[:,  -ndates_output:].copy() ## dynamic data used as ground truth labels/values (to be predicted)

## standardize dynamic data:
## we use the last 2 weeks of (available) data as typical value
referenceValues = Xd[:,-2:]
mean_Xd = referenceValues.mean(axis=1).mean(axis=0)
std_Xd  = referenceValues.std(axis=1).std(axis=0) # Xd[:,-2:].std(axis=1).std(axis=0)
things_useful_for_going_back_to_raw_numbers.append(mean_Xd)
things_useful_for_going_back_to_raw_numbers.append(std_Xd)

scaled_Xd = (Xd-mean_Xd)/std_Xd
ndates_input = scaled_Xd.shape[1]
scaled_Xd = scaled_Xd.reshape( (ndepartments, ndates_input*nvalues) )

## standardize output (ground truth) with same scales as input
scaled_yd = (yd-mean_Xd)/std_Xd ## we cannot know in advance the scaling factor of future data !!
ndates_output = scaled_yd.shape[1]
scaled_yd = scaled_yd.reshape( (ndepartments, ndates_output*nvalues))
# scaled_yd = scaled_yd.mean(axis=1) ## we average the next two weeks to make it less noisy
# ndates_output = 1


def reshape_with_time(y, ndates_output, nvalues):
    ## used later to restore the shape
    return y.reshape((y.shape[0], ndates_output, nvalues))

def raw_number(y, mean_Xd, std_Xd, pops, indexes, ndates_output, nvalues):
    ## used later to restore into before standardization format/numbers
    time_y = reshape_with_time(y,     ndates_output, nvalues) # restore the temporal aspect

    ## un-apply standardization
    de_standardized_y = (time_y*std_Xd+mean_Xd)

    ## restore true numbers (populations in absolute value)
    raw_number_y = np.zeros( de_standardized_y.shape )
    for i, dep in enumerate(indexes):
        raw_number_y[i] = de_standardized_y[i] * pops[dep]
    raw_number_y = de_standardized_y

    return raw_number_y



#############################
### build train+test sets ###

if inputType=="combined":
    ## combine static and dynamic input
    ## here this is a dumb way (nto accounting for the temporal specificity of our data)
    X = np.concatenate( (Xpca, scaled_Xd), axis=1) ## using both
elif inputType=="static-only":
    X = Xpca.copy()  ## only static !
elif inputType=="time-series-only" :
    X = scaled_Xd.copy() ## only dynamic (no socio-demographics)
else:
    print("inputType: combined, static-only, time-series-only")
    raise SystemExit

## output (to be predicted)
y = scaled_yd.copy()


def train_test_pop_split(X,y,test_ratio, seed):
    ## train-test split, KEEPING TRACK of the departmental populations ##
    rng = np.random.default_rng(seed)
    Nexamples = X.shape[0]
    indexes = np.arange(Nexamples, dtype=int)
    Ntest = int(Nexamples*test_ratio)
    test_indexes = rng.choice(indexes, size=Ntest, replace=False)
    train_indexes = []
    for ind in indexes:
        if ind not in test_indexes:
            train_indexes.append(ind)
    train_indexes = np.array(train_indexes)

    X_train= X[train_indexes]
    y_train= y[train_indexes]
#     y_train_pop = pop[train_indexes].reshape( (Nexamples-Ntest,1) )

    X_test = X[test_indexes]
    y_test = y[test_indexes]
#     y_test_pop = pop[test_indexes].reshape( (Ntest,1) )
    return X_train, X_test, y_train, y_test, train_indexes, test_indexes # , y_train_pop, y_test_pop


seed = 42
test_ratio=0.33
X_train, X_test, y_train, y_test, train_indexes, test_indexes = train_test_pop_split(X, y, test_ratio, seed)
#, y_train_pop, y_test_pop = train_test_pop_split(X, y, pop, test_ratio, seed)


########################################################################################################################

######################
### models (cheap) ###
n_jobs = 2
if modelType== 'mlp':
    # network_layers = (48,) #  bad
    network_layers = (480*2,48,24) # not very good
    ## impressively, these 2 architectures provide very similar results
    network_layers = (480*2,)
    network_layers = (480*2,48)
    # model = sklearn.neural_network.MLPRegressor(network_layers,  solver='lbfgs', max_iter=1000)
    model = sklearn.neural_network.MLPRegressor(network_layers, learning_rate='adaptive', early_stopping=True, validation_fraction=0.2,n_iter_no_change=20)
elif modelType== 'meta-mlp':
    network_layers = (48,) #  (480*2,48,24)
    # model = sklearn.neural_network.MLPRegressor(network_layers,  solver='lbfgs', max_iter=1000)
    model_single_output = sklearn.neural_network.MLPRegressor(network_layers, learning_rate='adaptive', early_stopping=True, validation_fraction=0.2,n_iter_no_change=20)
    model = sklearn.multioutput.MultiOutputRegressor(estimator=model_single_output,n_jobs=n_jobs)
elif modelType == 'svr':
    # model = sklearn.svm.LinearSVR()
    # model_single_output = sklearn.svm.SVR(kernel='poly', degree=2) # , C=1)
    # model_single_output = sklearn.svm.SVR(kernel='linear', C=0.0001)
    model = sklearn.multioutput.MultiOutputRegressor(estimator=model_single_output,n_jobs=n_jobs)
elif modelType == 'arima':

    scaled_Xd = (Xd-mean_Xd)/std_Xd
    # ndates_input = scaled_Xd.shape[1]
    endog = scaled_Xd[0]
    exog = Xpca[0]
    model = statsmodels.tsa.api.VAR(endog=endog , exog=exog ) # , order=[1,1,0])
    model.fit()
    # model = statsmodels.api.tsa.ARIMA(endog=endog , exog=exog, order=[1,1,0])

    pass
elif modelType == 'linreg':
    pass
elif modelType == 'rfr':
    max_depth=10
    n_estimators=100
    model = sklearn.ensemble.RandomForestRegressor(n_estimators=n_estimators, max_depth=max_depth, random_state=0)
elif modelType == 'meta-rfr':
    max_depth=5
    n_estimators=10
    model_single_output = sklearn.ensemble.RandomForestRegressor(n_estimators=n_estimators, max_depth=max_depth, random_state=0)
    model = sklearn.multioutput.MultiOutputRegressor(estimator=model_single_output,n_jobs=n_jobs)
    # model = sklearn.linear_model.LinearRegression(normalize=False)
else:
    pass
    raise SystemExit

# raise SystemExit

print("X_train.shape: ", X_train.shape, "\ny_train.shape:",y_train.shape)
model.fit(X=X_train, y=y_train)
print("model now trained")

## predictions
ypred_test     = model.predict(X_test)
ypred_train    = model.predict(X_train)
ypred_all      = model.predict(X)

print("score - all  : ", model.score(X,y, sample_weight=None) )
print("score - train: ", model.score(X_train,y_train, sample_weight=None) )
print("score - test : ", model.score(X_test,y_test, sample_weight=None) )


# time_ypred_test  = reshape_with_time(ypred_test,  ndates_output, nvalues)
# time_ypred_train = reshape_with_time(ypred_train, ndates_output, nvalues)
# time_ypred_all   = reshape_with_time(ypred_all,   ndates_output, nvalues)
# time_y_train     = reshape_with_time(y_train,     ndates_output, nvalues)
# time_y_test      = reshape_with_time(y_test,      ndates_output, nvalues)
# time_y           = reshape_with_time(y     ,      ndates_output, nvalues)

all_indexes = list(range(ndepartments))
raw_ypred_test  = raw_number(ypred_test , mean_Xd, std_Xd, pops, test_indexes, ndates_output, nvalues)
raw_ypred_train = raw_number(ypred_train, mean_Xd, std_Xd, pops, train_indexes, ndates_output, nvalues)
raw_ypred_all   = raw_number(ypred_all  , mean_Xd, std_Xd, pops, all_indexes, ndates_output, nvalues)
raw_y_train     = raw_number(y_train    , mean_Xd, std_Xd, pops, train_indexes, ndates_output, nvalues)
raw_y_test      = raw_number(y_test     , mean_Xd, std_Xd, pops, test_indexes, ndates_output, nvalues)
raw_y           = raw_number(y          , mean_Xd, std_Xd, pops, all_indexes, ndates_output, nvalues)

difftest = (raw_ypred_test-raw_y_test)/raw_y_test
diffall = (raw_ypred_all-raw_y.reshape((raw_y.shape[0], ndates_output, nvalues)))/raw_ypred_all
for indicator in range(24):
    errtest = np.median(np.abs(difftest[:, :, indicator]))
    errall = np.median(np.abs(diffall[:, :, indicator]))
    print( errtest , errall, dynamic_file_column_names[indicator])


# ## we show some departments, not all of the test set, for clarity
# Nshow=15

# import matplotlib.cm as cm
# Ncolors = Nshow+1
# gradient = cm.jet( np.linspace(0.0, 1.0, Ncolors+1 ) )
# # color = tuple(gradient[dep])

# for dep in range(Nshow):
#     color = tuple(gradient[dep])
#     plt.figure(1)
#     plt.semilogy(ytrue [dep], ls='-', lw=3, color=color, label= "true")
#     plt.plot(ypred [dep], ls=':', lw=2, color=color, label= "predicted")

#     plt.figure(2)
#     plt.loglog(ytrue [dep], ypred [dep], ls='', marker='x', markersize=5, color=color)
# plt.figure(2)
# plt.xlabel("ytrue")
# plt.ylabel("ypred")


