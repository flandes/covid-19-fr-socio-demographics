import logging
import urllib
from lxml import html
#import requests
import pandas as pd
import time


t0 = time.time()
def scrap_public_data(url, field_start, *argv): 
    ## if read file =='off' returne the name of the file.csv
    ## else retourn the pandas dataframe

    # field_name = "donnees-hospitalieres-covid19-2020"

    with urllib.request.urlopen(url) as f:
        html_content = f.read().decode("utf8")
        #print('html_content', html_content)
    tree = html.fromstring(html_content)
    download_url = None
    fl = 1
    elements = tree.xpath('//*[contains(@class, "resource-card")]')
    for e in elements:
        if fl==0:
            break
        resource_names = e.xpath('//*[contains(@class, "ellipsis")]/text()')#[0]
        for resource_name in resource_names:
            if not argv:
                if resource_name.startswith(field_start):
                    download_url = e.xpath("//*[@download]/@href")[0]
                    print("found resource %s at %s" % (resource_name, download_url))
                    fl=0
                    break
            else: 
                if resource_name.startswith(field_start) and resource_name.endswith(argv[0]):
                    download_url = e.xpath("//*[@download]/@href")[0]
                    print("found resource %s at %s" % (resource_name, download_url))
                    fl=0
                    break
           
    if download_url is None:
        print('Error Scarping data')
        print("If error presists try to donwload manually the file ",  field_start, '..*.csv in the url', url, ' and save in temp folder')
        
        raise Exception("Error in scrap public data")
      
    return resource_name, download_url


### CODE OF GENERATING FILE

import glob
import pandas as pd
import math

def name_dep_dic(text):
    ### if tetx in department code
    if text in  dic_code_dep.keys():
        return dic_code_dep[text]
    else:
        print('Warning ', text)
        return ''

def add_name_depart(datafr , colum_dep ):
    ## colum_cod, a list of posible integers
    ## add column of department
    datafr['DEPARTMENT'] =''
    datafr['DEPARTMENT'] = datafr[colum_dep].apply(name_dep_dic)
    return datafr

def generate_reduced_dat(df, key, value):## if key ==sexe filter
    ## possible column == hosp,rea,rad,dc, retourn only one column dates ans department
    
    if value in list(set(df[key])):
        return df[df[key]==value]
    else:
        raise Exception(value,' , ', key, ' not presnt')


def my_isnan(x):
    try:
        return math.isnan(x)
    except TypeError:
        return False
    
def change_order_col(dataframe, colum, date_nam='jour'): ## date_nam 'jour'
    dic_res= {}
    #filter nan
    lst_dep = set(dataframe['dep'])
    lst_dep = {x for x in lst_dep if not my_isnan(x)}
    lst_dep = sorted(lst_dep)
    for el in lst_dep:
        dfes =dataframe[dataframe['dep']==el]
        #a= dfes['jour'].values.tolist()
        a= dfes[date_nam].values.tolist()
        
        b= dfes[colum].values.tolist()
        dic_res[el]=[a,b]    
    return dic_res

def add_date_columns(df3,df6, dic_res, date_nam='jour'): ## date_nam 'jour, D6 department list
    sort_dat = sorted(set(df6[date_nam]))
    for date_c in sort_dat:
        df3[date_c] = 'nan' #np.nan
        for id_l in df3.index:
            dep = df3.loc[id_l,'code']
            if date_c in dic_res[dep][0] :
                indx_dic = dic_res[dep][0].index(date_c)
                df3.loc[id_l,date_c] = dic_res[dep][1][indx_dic]
    return df3

def select_candidate_columns(df, list_columns): ## list of columns example = ['hosp', 'rea', 'rad', 'dc']
    list_colum_dat2 =[]
    print('df.columns',df.columns)
    ## filter columns  ['dep', 'sexe', 'jour']
    #list_colum_dat = [el for el in df.columns if el not in ['dep', 'sexe', 'jour']]
    #list_colum_dat2 = [el for el in list_colum_dat  if df[el].dtype == int or df[el].dtype == float]
    for el in list_columns: ## assure tthe presence of this name if are preetns
        if (el in df.columns):
            list_colum_dat2.append(el)
        else:
            raise Exception(el, ' not in columns')
    return list(set(list_colum_dat2))


def generate_name_csv_file(el, dict_metad, dic_value_txt, filter_value, text_name_to_add = 'donnees-hospitalieres_'):
    ## for el like ['rad', 'hosp', 'rea', 'dc'] >> 
    if el in dict_metad.keys():
        return 'TIME_serie_' + text_name_to_add + '_'.join(dict_metad[el].split())+ dic_value_txt[filter_value] +'.csv'
    else:
        logging.info('name_not_present')
        
        return '_'

def main_generate_fil(df,colum, path_out,filter_key,filter_value,  date_nam  ,file_nam_csv): # dict_metad, text_name_to_add  = 'donnees-hospitalieres_'): ## date_nam='jour'
    ## generate file
    #df4 = generate_red_dat(df,sexe=sexe_cod)
    df4 = generate_reduced_dat(df, filter_key, filter_value)
    dic_res = change_order_col(df4, colum, date_nam)
    dep_cod = dp[['dep']]
    dfin=dep_cod.rename(columns ={'dep': 'code'})
    df3 = add_name_depart(dfin , colum_dep='code' )
    
    df7 = add_date_columns(df3,df,dic_res, date_nam)
    
    #file_nam = generate_name_csv_file(colum, dict_metad, text_name_to_add)
    df7.to_csv(path_out + file_nam_csv )
    print('file created: ', file_nam_csv)

## DICTIONARY OF NAME OF DEPARTSMENTS

## ignore the add the nme of the deparment 

path_dep = "metadata/"
file = 'departement2019.csv'
dp= pd.read_csv(path_dep + file, sep =',')

dic_code_dep= {}
for i, j in dp.iterrows():
    dic_code_dep[j['dep']] = j['ncc']
dp.head()
pd.DataFrame.from_dict(dic_code_dep, orient='index')
dp6 = dp[['dep','ncc']]





