from shared_code.common_code import *  

def read_donnees_urgences():
    url = "https://www.data.gouv.fr/fr/datasets/donnees-des-urgences-hospitalieres-et-de-sos-medecins-relatives-a-lepidemie-de-covid-19/"
    text_start = "sursaud-covid19-quotidien"#-2020-04-26-19h00-departement.csv"
    text_end = "departement.csv"
    #text_end = "region.csv"
    csv_file_name, download_url = scrap_public_data(url, text_start, text_end)
    df = pd.read_csv(download_url, sep=",")
    return df

dict_metad ={}

metadones_urgences = 'metadata/METADATA_SOS/metadonnee-urgenceshos-sosmedecin-covid19-quot.csv'
urgences = pd.read_csv(metadones_urgences, skiprows=1,sep=";")
for ind_ in urgences.index:
    dict_metad[urgences.loc[ind_,'Colonne']] = urgences.loc[ind_,'Description_EN']


df = read_donnees_urgences()

dic_urg ={ 'date_nam' :'date_de_passage', 'filter_key': 'sursaud_cl_age_corona' , 'prefix_file' : 'Urgences-' }

            
def generate_donnees_urgences(dic_value_txt, list_val, files_time_series, path_out):
    for filter_value in list_val:
        for time_serie in select_candidate_columns(df , files_time_series  ):## Create files
            #main_generate_fil(df,colum, path_out, filter_key,filter_value ) #sexe_od)
            name_csv_file  = generate_name_csv_file(time_serie, dict_metad, dic_value_txt, filter_value, dic_urg['prefix_file'])
            print(name_csv_file)  
            main_generate_fil(df, time_serie , path_out,  dic_urg['filter_key'], filter_value, dic_urg['date_nam'], name_csv_file)



