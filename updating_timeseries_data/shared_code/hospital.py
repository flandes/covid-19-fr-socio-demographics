from shared_code.common_code import *  


def read_donnees_hospitalieres():
    url = "https://www.data.gouv.fr/fr/datasets/donnees-hospitalieres-relatives-a-lepidemie-de-covid-19/"
    text_start = "donnees-hospitalieres-covid19-2020"
    #url = "https://www.data.gouv.fr/fr/datasets/donnees-des-urgences-hospitalieres-et-de-sos-medecins-relatives-a-lepidemie-de-covid-19/"
    csv_file_name, download_url =scrap_public_data(url, text_start)
    df = pd.read_csv(download_url, sep=";")
    return df


df = read_donnees_hospitalieres()
#df = read_donnees_urgences()

dict_metad={}
metadones_donnes_hospitalaires = 'metadata/METADATA_hospitaliers/metadonnees-donnees-hospitalieres-covid19.csv'
df_hosp = pd.read_csv(metadones_donnes_hospitalaires, sep=";")
for ind_ in df_hosp[3:].index:
    dict_metad[ df_hosp.loc[ind_,'Colonne']] =  df_hosp.loc[ind_,'Description_EN']


dic_urg ={ 'date_nam' :'jour', 'filter_key': 'sexe' , 'prefix_file' : 'donnees-hospitalieres-' }

def generate_donnees_hosp_dataset(dic_value_txt, list_val, files_time_series, path_out):
    for filter_value in list_val:
        for time_serie in select_candidate_columns(df , files_time_series  ):## Create files
            #main_generate_fil(df,colum, path_out, filter_key,filter_value ) #sexe_od)
            name_csv_file  = generate_name_csv_file(time_serie, dict_metad, dic_value_txt, filter_value, dic_urg['prefix_file'])
            print(name_csv_file)  
            main_generate_fil(df, time_serie , path_out,  dic_urg['filter_key'], filter_value, dic_urg['date_nam'], name_csv_file)




