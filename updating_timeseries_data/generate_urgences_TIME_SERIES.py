
from shared_code.common_code import *  

from shared_code.urgences import *
          
            
dict_metad = {
'nbre_pass_corona'  :  'Number of emergency room visits for suspicion of COVID-19',
'nbre_pass_tot'  :  'Total amount of emergency room visits',
'nbre_hospit_corona'  :  'Number of hospitalizations among emergency department visits for suspicion of COVID-19', 
'nbre_pass_corona_h'  :  'Number of emergency room visits for suspicion of COVID-19 - Males',
'nbre_pass_corona_f'  :  'Number of emergency room visits for suspicion of COVID-19  - Females',
'nbre_pass_tot_h'  :  'Total amount of emergency room visits - Males',
'nbre_pass_tot_f'  :  'Total amount of emergency room visits - Females',
'nbre_hospit_corona_h'  :  'Number of hospitalizations among emergency department visits for suspicion of COVID-19  - Males',
'nbre_hospit_corona_f'  :  'Number of hospitalizations among emergency department visits for suspicion of COVID-19  - Males',
'nbre_acte_corona'  :  'Number of medical acts (SOS Médecin) for suspicion of COVID-19',
'nbre_acte_tot'  :  'Total amount of medical acts (SOS Médecin)',
'nbre_acte_corona_h'  :  'Number of medical acts (SOS Médecin) for suspicion of COVID-19 - Males',
'nbre_acte_corona_f'  :  'Number of medical acts (SOS Médecin) for suspicion of COVID-19 - Females',
'nbre_acte_tot_h'  :  'Total amount of medical acts (SOS Médecin) - Males',
'nbre_acte_tot_f'  :  'Total amount of medical acts (SOS Médecin) - Females'
}

dic_value_txt = {
'0' : '_age_all',
'A' :'_age<15',
'B' : '_age_15-44',
'C': '_age_45-64',
'D' : '_age_65-74',
'E' : '_age>75'
}

"""
## POSIBLE COMBINATIONS OF

files_time_series = ['nbre_pass_corona',
 'nbre_pass_tot',
 'nbre_hospit_corona',
 'nbre_pass_corona_h',
 'nbre_pass_corona_f',
 'nbre_pass_tot_h',
 'nbre_pass_tot_f',
 'nbre_hospit_corona_h',
 'nbre_hospit_corona_f',
 'nbre_acte_corona',
 'nbre_acte_tot',
 'nbre_acte_corona_h',
 'nbre_acte_corona_f',
 'nbre_acte_tot_h',
 'nbre_acte_tot_f']

files_time_series = ['0', 'A', 'B', 'C', 'D', 'E']

######################################
"""
path_out = '../data/'
files_time_series = ['nbre_acte_corona' , 'nbre_pass_tot', 'nbre_acte_tot_h']
list_val = ['0','E']

generate_donnees_urgences(dic_value_txt, list_val, files_time_series, path_out)

