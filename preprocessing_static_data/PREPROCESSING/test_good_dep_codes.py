import glob
import pandas as pd

## DICTIONARY OF NAME OF DEPARTSMENTS
# path_dep = "../../data_raw/department_static_data/IRIS/"
# path_dep = "../preprocess_static_code/"
file = 'departement2019.csv'
dp= pd.read_csv(file, sep =',')

dic_code_dep= {}
for i, j in dp.iterrows():
    dic_code_dep[j['dep']] = j['ncc']

def test_completitude_departments(colum_val , dic_code_dep= dic_code_dep):
    dic = {}
    error_=[]
    for el in colum_val:
        #print('el ', el, type(el))

        if el in dic.keys():
            print('repated element', el)
            error_.append(el)
        else:
            dic[el]=1
    for el in  list(dic_code_dep.keys()):
        if el not in list(dic.keys()) :
            print('not present element', el)
            error_.append(el)
    if len(error_)==0:
        print('TEST PASSED. Good department codes')
