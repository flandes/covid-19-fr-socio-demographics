
found at:
https://www.insee.fr/fr/statistiques/4228434#consulter

link to data:
https://www.insee.fr/fr/statistiques/fichier/4228434/base-ic-evol-struct-pop-2016.zip


------------------------------------------------------------------------------------------
documentation:

https://www.insee.fr/fr/statistiques/4228434#dictionnaire
---------------------------------------------------------

Population en 2016
Recensement de la population - Base infracommunale (IRIS)

https://www.insee.fr/fr/statistiques/4228434#dictionnaire


Géographie (au 01/01/2018)

    IRIS : code du département suivi du numéro de commune ou du numéro d'arrondissement municipal suivi du numéro d'IRIS
    REG : code de la région
    DEP : code du département
    UU2010 : code du département ou "00" pour les unités urbaines qui s'étendent sur plusieurs départements voire au-delà de la frontière suivi d'un code sur une position indiquant la taille de la population puis d'un numéro d'ordre à l'intérieur de la taille
    COM : code du département suivi du numéro de commune ou du numéro d'arrondissement municipal pour Paris Lyon et Marseille
    LIBCOM : libellé de la commune ou de l'arrondissement municipal pour Paris Lyon et Marseille
    TRIRIS : code du département suivi d'un numéro d'ordre à l'intérieur du département sur trois positions puis d'un indicateur de TRIRIS
    GRD_QUART : code du département suivi du numéro de commune ou du numéro d'arrondissement municipal pour Paris Lyon et Marseille suivi du numéro de grand quartier
    LIBIRIS : libellé de l'IRIS à l'intérieur de la commune ou de l'arrondissement municipal pour Paris Lyon et Marseille
    TYP_IRIS : type d'IRIS : habitat (H), activité (A), divers (D), Autre (Z)
    MODIF_IRIS : type de modification de l'IRIS
    LAB_IRIS : label de qualité de l'IRIS

Population

    P16_POP : population
    P16_POP0002 : nombre de personnes de 0 à 2 ans
    P16_POP0305 : nombre de personnes de 3 à 5 ans
    P16_POP0610 : nombre de personnes de 6 à 10 ans
    P16_POP1117 : nombre de personnes de 11 à 17 ans
    P16_POP1824 : nombre de personnes de 18 à 24 ans
    P16_POP2539 : nombre de personnes de 25 à 39 ans
    P16_POP4054 : nombre de personnes de 40 à 54 ans
    P16_POP5564 : nombre de personnes de 55 à 64 ans
    P16_POP6579 : nombre de personnes de 65 à 79 ans
    P16_POP80P : nombre de personnes de 80 ans ou plus
    P16_POP0014 : nombre de personnes de 0 à 14 ans
    P16_POP1529 : nombre de personnes de 15 à 29 ans
    P16_POP3044 : nombre de personnes de 30 à 44 ans
    P16_POP4559 : nombre de personnes de 45 à 59 ans
    P16_POP6074 : nombre de personnes de 60 à 74 ans
    P16_POP75P : nombre de personnes de 75 ans ou plus
    P16_POP0019 : nombre de personnes de 0 à 19 ans
    P16_POP2064 : nombre de personnes de 20 à 64 ans
    P16_POP65P : nombre de personnes de 65 ans ou plus
    P16_PMEN : population des ménages
    P16_PHORMEN : population hors ménages

Caractéristiques des personnes

    C16_POP15P : nombre de personnes de 15 ans ou plus
    C16_POP15P_CS1 : nombre de personnes de 15 ans ou plus Agriculteurs exploitants
    C16_POP15P_CS2 : nombre de personnes de 15 ans ou plus Artisans, Commerçants, Chefs d'entreprise
    C16_POP15P_CS3 : nombre de personnes de 15 ans ou plus Cadres et Professions intellectuelles supérieures
    C16_POP15P_CS4 : nombre de personnes de 15 ans ou plus Professions intermédiaires
    C16_POP15P_CS5 : nombre de personnes de 15 ans ou plus Employés
    C16_POP15P_CS6 : nombre de personnes de 15 ans ou plus Ouvriers
    C16_POP15P_CS7 : nombre de personnes de 15 ans ou plus Retraités
    C16_POP15P_CS8 : nombre de personnes de 15 ans ou plus Autres sans activité professionnelle
    P16_POP_FR : nombre de personnes de nationalité française
    P16_POP_ETR : nombre de personnes étrangères
    P16_POP_IMM : nombres de personnes immigrées

Caractéristiques des femmes

    P16_POPF : nombre de femmes
    P16_F0014 : nombre de femmes de 0 à 14 ans
    P16_F1529 : nombre de femmes de 15 à 29 ans
    P16_F3044 : nombre de femmes de 30 à 44 ans
    P16_F4559 : nombre de femmes de 45 à 59 ans
    P16_F6074 : nombre de femmes de 60 à 74 ans
    P16_F75P : nombre de femmes de 75 ans ou plus
    P16_F0019 : nombre de femmes de 0 à 19 ans
    P16_F2064 : nombre de femmes de 20 à 64 ans
    P16_F65P : nombre de femmes de 65 ans ou plus
    C16_F15P : nombre de femmes de 15 ans ou plus
    C16_F15P_CS1 : nombre de femmes de 15 ans ou plus Agriculteurs exploitants
    C16_F15P_CS2 : nombre de femmes de 15 ans ou plus Artisans, Commerçants, Chefs d'entreprise
    C16_F15P_CS3 : nombre de femmes de 15 ans ou plus Cadres et Professions intellectuelles supérieures
    C16_F15P_CS4 : nombre de femmes de 15 ans ou plus Professions intermédiaires
    C16_F15P_CS5 : nombre de femmes de 15 ans ou plus Employés
    C16_F15P_CS6 : nombre de femmes de 15 ans ou plus Ouvriers
    C16_F15P_CS7 : nombre de femmes de 15 ans ou plus Retraités
    C16_F15P_CS8 : nombre de femmes de 15 ans ou plus Autres sans activité professionnelle

Caractéristiques des hommes

    P16_POPH : nombre d'hommes
    P16_H0014 : nombre d'hommes de 0 à 14 ans
    P16_H1529 : nombre d'hommes de 15 à 29 ans
    P16_H3044 : nombre d'hommes de 30 à 44 ans
    P16_H4559 : nombre d'hommes de 45 à 59 ans
    P16_H6074 : nombre d'hommes de 60 à 74 ans
    P16_H75P : nombre d'hommes de 75ans ou plus
    P16_H0019 : nombre d'hommes de 0 à 19 ans
    P16_H2064 : nombre d'hommes de 20 à 64 ans
    P16_H65P : nombre d'hommes de 65 ans ou plus
    C16_H15P : nombre d'hommes de 15 ans ou plus
    C16_H15P_CS1 : nombre d'hommes de 15 ans ou plus Agriculteurs exploitants
    C16_H15P_CS2 : nombre d'hommes de 15 ans ou plus Artisans, Commerçants, Chefs d'entreprise
    C16_H15P_CS3 : nombre d'hommes de 15 ans ou plus Cadres et Professions intellectuelles supérieures
    C16_H15P_CS4 : nombre d'hommes de 15 ans ou plus Professions intermédiaires
    C16_H15P_CS5 : nombre d'hommes de 15 ans ou plus Employés
    C16_H15P_CS6 : nombre d'hommes de 15 ans ou plus Ouvriers
    C16_H15P_CS7 : nombre d'hommes de 15 ans ou plus Retraités
    C16_H15P_CS8 : nombre d'hommes de 15 ans ou plus Autres sans activité professionnelle


